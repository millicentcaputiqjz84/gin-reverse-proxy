package main

import (
	"fmt"
	"log"
	"net/netip"
	"net/url"
	"os"
	"runtime"

	"github.com/gin-gonic/gin"
	"github.com/valyala/fasthttp"
)

var relayURI = os.Getenv("RP_RELAY_URI") //nolint:gochecknoglobals

func main() {
	ConfigRuntime()
	runSrv()
}

// ConfigRuntime sets the number of operating system threads.
func ConfigRuntime() {
	nuCPU := runtime.NumCPU()
	runtime.GOMAXPROCS(nuCPU)
	fmt.Printf("Running with %d CPUs\n", nuCPU)
}

func runSrv() {
	gin.SetMode(gin.ReleaseMode)
	srv := gin.Default()
	srv.Any("/*proxyPath", relay)

	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}

	if err := srv.Run(":" + port); err != nil {
		log.Panicf("error: %s", err)
	}
}

func relay(ctx *gin.Context) {
	origin := ctx.Request

	uri, err := url.Parse(relayURI + origin.RequestURI)
	if err != nil {
		log.Print(err.Error())

		return
	}

	rBody, err := ctx.GetRawData()
	if err != nil {
		log.Print(err.Error())

		return
	}

	req := fasthttp.AcquireRequest()
	req.SetRequestURI(uri.String())
	req.SetBodyRaw(rBody)

	for k, v := range origin.Header {
		req.Header.Set(k, v[0])
	}

	req.Header.SetMethodBytes([]byte(origin.Method))

	ip, err := netip.ParseAddrPort(origin.RemoteAddr)
	if err != nil {
		log.Print(err)

		return
	}

	req.Header.Set("XF-Gateway-Real-IP", ip.String())

	resp := fasthttp.AcquireResponse()
	err = fasthttp.Do(req, resp)

	if err != nil {
		log.Print(err.Error())

		return
	}

	resp.Header.VisitAll(func(key, value []byte) {
		ctx.Header(string(key), string(value))
	})

	ctx.Data(resp.StatusCode(), ctx.GetHeader("Content-Type"), resp.Body())
}
